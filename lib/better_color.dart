library better_color;

import 'package:flutter/painting.dart';

/// Extensions for easily handling a [Color] as a [HSVColor] and also includes
/// shortcut helpers for some common operations.
extension BetterColor on Color {
  /// Convert this [Color] to an [HSVColor].
  HSVColor toHSV() => HSVColor.fromColor(this);

  /// Set the hue of this [Color]. 0 <= hue <= 1 must be true.
  Color withHue(double hue) => this.toHSV().withHue(hue).toColor();

  /// Set the saturation of this [Color]. 0 <= saturation <= 1 must be true.
  Color withSaturation(double saturation) =>
      this.toHSV().withSaturation(saturation).toColor();

  /// Set the brightness of this [Color]. 0 <= value <= 1 must be true.
  Color withValue(double value) => this.toHSV().withValue(value).toColor();

  /// Get the brightest [Color] with the same hue and saturation.
  Color get brighten => this.toHSV().withValue(1).toColor();

  /// Get the darkest [Color] with the same hue and saturation.
  Color get darken => this.toHSV().withValue(0).toColor();

  /// Rotate the hue value of this [Color] with the value of [spinValue]. 
  Color hueSpin(num spinValue) {
    final hsvColor = this.toHSV();
    return hsvColor.withHue((hsvColor.hue + 30) % 360.0).toColor();
  }
}
